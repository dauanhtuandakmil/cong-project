import { InjectionToken } from '@angular/core';
import { environment } from '../environments/environment';

export interface IAppConfig {
    apiEndpoint: string;
}

export const AppConfig: IAppConfig = {
    apiEndpoint: `${environment.apiUrl}`,
};
export const APICONFIG = {
    USER: {
        GETLISTUSER:'/api/taikhoan'
    }
}
export let APP_CONFIG = new InjectionToken<IAppConfig>('app.config');