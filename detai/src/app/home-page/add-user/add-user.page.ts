import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserProvider } from 'src/provider';
import { Router, ActivatedRoute } from '@angular/router';
import { isUndefined } from 'util';
import { NavController,AlertController } from '@ionic/angular';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.page.html',
  styleUrls: ['./add-user.page.scss'],
})
export class AddUserPage implements OnInit {
  public form: FormGroup;
  id: string;
  isId:string;
  constructor(
    private fb: FormBuilder,
    public userprovice: UserProvider,
    public router: Router,
    private route: ActivatedRoute, 
    private nav: NavController,
    public alertCtrl: AlertController
  ) {
    this.nav = nav;
   }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      if(isUndefined(params.id) === false) {
        this.id = params.id;
        this.editForm();
        this.getDetail()
      } else {
        this.addForm();
      }
    });
  }
  getDetail(){
    this.userprovice.getUserById(this.id).subscribe(data =>{
      this.form.patchValue({
        id: data.matk,
        user: data.username,
        pass: data.password,
      })
    })
  }
  editForm(){
      this.form = this.fb.group({
        id: [0, Validators.required],
        user: ['',Validators.required],
        pass: ['', Validators.required],
      })
  }
  async showAlert(title:string) { 
    const alert = await this.alertCtrl.create({ 
    header: 'Cảnh Báo', 
    message: title, 
    buttons: ['Đồng ý'] 
    }); 
    await alert.present(); 
    } 
  addForm(){
    this.form = this.fb.group({
        id:[0,[Validators.required]],
        user: ['', [Validators.required]],
        pass: ['', [Validators.required]],
    })
  }
   async save(){
    const data = {}
    this.userprovice.getUserById(this.form.value.id).subscribe(data =>{
      this.isId = data.matk;
      if(data.matk){
       this.showAlert('Id đã tồn tại!, vui lòng thử lại');
      }
    })
    this.userprovice.createUser(data,this.form.value.id, this.form.value.user, this.form.value.pass).subscribe(data =>{
        this.router.navigateByUrl('/home-page')
    })
  }
  saveChange(){
    const data = {}
    this.userprovice.updateUser(data,this.form.value.id, this.form.value.user, this.form.value.pass).subscribe(data =>{
      this.router.navigateByUrl('/home-page')
    })
  }
  async delete(){
    const alert = await this.alertCtrl.create({ 
      header: 'Cảnh báo', 
      message: 'Bạn chắc chắn muốn xóa user này?', 
      buttons: [
        { 
          text: 'Hủy',
          role: 'Cancel', 
          handler: () => {  
          } 
          }, 
          { 
          text: 'Đồng ý', 
          role: 'ok',
          handler: () => { 
            this.userprovice.deleteUser(this.id).subscribe(data =>{
              this.router.navigateByUrl('/home-page')
            })
          } 
          } 
      ] 
      }); 
      await alert.present(); 

   
  }
  cancel(){
    this.router.navigateByUrl('/home-page')
  }
}
