import { Component, OnInit } from '@angular/core';
import { UserProvider } from 'src/provider';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.page.html',
  styleUrls: ['./home-page.page.scss'],
})
export class HomePagePage implements OnInit {
  public idUser: string;
  constructor(
    public userservice: UserProvider,
    public router: Router
  ) { }
    dataUser;
  ngOnInit() {
    this.getlistUser();
  }
  userdetail(id:string){
    console.log('a');
    this.idUser = id;
    this.router.navigate(['/home-page/add-user/'+id]);
  }
  getlistUser(){
    this.userservice.getListUser().subscribe(data =>{
      this.dataUser = data;
    })
  }
  adduser(){
    this.router.navigateByUrl('/home-page/add-user');
  }
  ionViewWillEnter(){
    this.getlistUser();
  }
}
