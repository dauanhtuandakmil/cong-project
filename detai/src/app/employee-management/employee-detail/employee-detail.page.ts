import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserProvider } from 'src/provider';
import { Router, ActivatedRoute } from '@angular/router';
import { isUndefined, log } from 'util';
import { ToastController,NavController,AlertController } from '@ionic/angular';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.page.html',
  styleUrls: ['./employee-detail.page.scss'],
})
export class EmployeeDetailPage implements OnInit {
  public form: FormGroup;
  id: string;
  datatime:string = "";
  constructor(
    private fb: FormBuilder,
    public userprovice: UserProvider,
    public router: Router,
    private route: ActivatedRoute, 
    private nav: NavController,
    public alertCtrl: AlertController,
    private toastCtrl: ToastController
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      if(isUndefined(params.id) === false) {
        this.id = params.id;
        this.editForm();
        this.getDetail()
      } else {
        this.addForm();
      }
    });
  }
  getDetail(){
    this.userprovice.getEmployeeById(this.id).subscribe(data =>{
     console.log('data',data);
     data.forEach(element => {
      this.form.patchValue({
        maNv:element.MaNv,
        hoTen:element.HoTen,
        danToc: element.DanToc,
        gioiTinh: element.GioiTinh,
        diaChi: element.DiaChi,
        ngaySinh: element.NgaySinh,
        soDT: element.SoDT,
        maCv: element.MaCV,
        maPb: element.MaPB,
        bacLuong: element.LuongCB,
      })
      this.id = element.MaNv;
     });
    })
  }
  editForm(){
    this.form = this.fb.group({
      maNv:['',[Validators.required]],
      hoTen: ['', [Validators.required]],
      danToc: ['', [Validators.required]],
      gioiTinh: ['', [Validators.required]],
      diaChi: ['', [Validators.required]],
      ngaySinh: ['', [Validators.required]],
      soDT: ['', [Validators.required]],
      maCv: ['', [Validators.required]],
      maPb: ['', [Validators.required]],
      bacLuong: ['', [Validators.required]],
    })
}
  addForm(){
    this.form = this.fb.group({
      maNv:['',[Validators.required]],
      hoTen: ['', [Validators.required]],
      danToc: ['', [Validators.required]],
      gioiTinh: ['', [Validators.required]],
      diaChi: ['', [Validators.required]],
      ngaySinh: ['', [Validators.required]],
      soDT: ['', [Validators.required]],
      maCv: ['', [Validators.required]],
      maPb: ['', [Validators.required]],
      bacLuong: ['', [Validators.required]],
    })
  }
  async save(){
    const data = {}
    
    this.userprovice.createEmployee(data,this.form.value.maNv, this.form.value.hoTen, this.form.value.danToc,this.form.value.gioiTinh,this.form.value.diaChi, this.form.value.ngaySinh,this.form.value.soDT,this.form.value.maCv,this.form.value.maPb,this.form.value.bacLuong).subscribe(data =>{
      this.router.navigateByUrl('/employee-management')
    })
  }
  saveChange(){
    const data = {}
    this.userprovice.updateEmployee(data,this.form.value.maNv, this.form.value.hoTen, this.form.value.danToc,this.form.value.gioiTinh,this.form.value.diaChi, this.form.value.ngaySinh,this.form.value.soDT,this.form.value.maCv,this.form.value.maPb,this.form.value.bacLuong).subscribe(data =>{
      this.router.navigateByUrl('/employee-management')
    })
  }
  async delete(){
    const alert = await this.alertCtrl.create({ 
      header: 'Cảnh báo', 
      message: 'Bạn chắc chắn muốn xóa nhân viên này?', 
      buttons: [
        { 
          text: 'Hủy',
          role: 'Cancel', 
          handler: () => {  
          } 
          }, 
          { 
          text: 'Đồng ý', 
          role: 'ok',
          handler: () => { 
            this.userprovice.deleteEmployee(this.id).subscribe(data =>{
              this.router.navigateByUrl('/employee-management')
            })
          } 
          } 
      ] 
      }); 
      await alert.present(); 

   
  }
  cancel(){
    this.router.navigateByUrl('/employee-management')
  }
}
