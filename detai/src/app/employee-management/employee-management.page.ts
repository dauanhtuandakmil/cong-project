import { Component, OnInit } from '@angular/core';
import { UserProvider } from 'src/provider';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-management',
  templateUrl: './employee-management.page.html',
  styleUrls: ['./employee-management.page.scss'],
})
export class EmployeeManagementPage implements OnInit {
  dataUser;
  public idEmployee: string;

  constructor(
    private userprovider:UserProvider,
    private router: Router
  ) { }

  ngOnInit() {
    this.getlistEmployee();
  }
  ionViewWillEnter(){
    this.getlistEmployee();
  }
  getlistEmployee(){
    this.userprovider.getListEmployee().subscribe(data =>{
      this.dataUser = data;
      console.log(this.dataUser);
      
    })
  }
  employeedetail(id:string){
    this.idEmployee = id;
    this.router.navigate(['/employee-management/employee-detail/'+id]);
  }
  adduser(){
    this.router.navigateByUrl('/employee-management/employee-detail');
  }
}
