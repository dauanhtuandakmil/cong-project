import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { APICONFIG } from '../../app/app.config';

@Injectable()

export class UserProvider {
    constructor(public http: HttpClient) { }
    
    public getListUser():Observable<any>{
        return this.http.get(`${environment.apiUrl}/api/taikhoan`);
    }
    public getUserById(id:string):Observable<any>{
        return this.http.get(`${environment.apiUrl}/api/taikhoan?id=${id}`);
    }
    public createUser(body, id: string, user: string, pass: string):Observable<any>{
        return this.http.post(`${environment.apiUrl}/api/taikhoan?id=${id}&user=${user}&pass=${pass}`,body);
    }
    public deleteUser(id:string):Observable<any>{
        return this.http.delete(`${environment.apiUrl}/api/TaiKhoan/${id}`);
    }
    public updateUser(body, matk: string, user: string, pass: string):Observable<any>{
        return this.http.put(`${environment.apiUrl}/api/taikhoan?matk=${matk}&user=${user}&pass=${pass}`,body);
    }

    public getListEmployee():Observable<any>{
        return this.http.get(`${environment.apiUrl}/api/NhanVien`);
    }
    public getEmployeeById(id:string):Observable<any>{
        return this.http.get(`${environment.apiUrl}/api/NhanVien/${id}`);
    }
    public createEmployee(body,maNv:string, hoTen:string, danToc:string, gioiTinh:string,diaChi:string,ngaySinh:string,soDT:string,maCv:string,maPb:string,bacLuong:string):Observable<any>{
        return this.http.post(`${environment.apiUrl}/api/NhanVien?maNv=${maNv}&hoTen=${hoTen}&danToc=${danToc}&gioiTinh=${gioiTinh}&diaChi=${diaChi}&ngaySinh=${ngaySinh}&soDT=${soDT}&maCv=${maCv}&maPb=${maPb}&bacLuong=${bacLuong}`,body);
    }
    public updateEmployee(body, maNv:string, hoTen:string, danToc:string, gioiTinh:string,diaChi:string,ngaySinh:string,soDT:string,maCv:string,maPb:string,bacLuong:string):Observable<any>{
        return this.http.put(`${environment.apiUrl}/api/NhanVien?maNv=${maNv}&hoTen=${hoTen}&danToc=${danToc}&gioiTinh=${gioiTinh}&diaChi=${diaChi}&ngaySinh=${ngaySinh}&soDT=${soDT}&maCv=${maCv}&maPb=${maPb}&bacLuong=${bacLuong}`,body);
    }
    public deleteEmployee(id:string):Observable<any>{
        return this.http.delete(`${environment.apiUrl}/api/NhanVien?maNv=${id}`);
    }
}